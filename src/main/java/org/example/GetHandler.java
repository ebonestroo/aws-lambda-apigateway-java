package org.example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPResponse;
import com.google.gson.Gson;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanResponse;

import java.util.*;
import java.util.stream.Collectors;

public class GetHandler implements RequestHandler<APIGatewayV2HTTPEvent, APIGatewayV2HTTPResponse> {
    private static final String table = System.getenv("TABLE_NAME");

    DynamoDbClient ddb = DynamoDbClient.builder()
            .region(Region.EU_CENTRAL_1)
            .build();
    @Override
    public APIGatewayV2HTTPResponse handleRequest(APIGatewayV2HTTPEvent event, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("EVENT TYPE: "+ event.getClass());


        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        List<HelloWorldResponse> helloWorldResponses = scanItems(ddb, table);

        APIGatewayV2HTTPResponse build = APIGatewayV2HTTPResponse.builder()
                .withHeaders(headers)
                .withStatusCode(200)
                .withIsBase64Encoded(false)
                .withBody(new Gson().toJson(helloWorldResponses))
                .build();

        return build;
    }

    public static List<HelloWorldResponse> scanItems(DynamoDbClient ddb, String tableName ) {

        try {
            ScanRequest scanRequest = ScanRequest.builder()
                    .tableName(tableName)
                    .build();

            ScanResponse response = ddb.scan(scanRequest);
            return response.items()
                    .stream()
                    .map(item -> new HelloWorldResponse(item.get("name").s()))
                    .collect(Collectors.toList());

        } catch (DynamoDbException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return new ArrayList();
    }
}
