package org.example;

public class HelloWorldResponse {
    private String response;

    public HelloWorldResponse(String response) {
        this.response = response;
    }

    public HelloWorldResponse() {
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
