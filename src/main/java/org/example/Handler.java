package org.example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPResponse;
import com.google.gson.Gson;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.*;

import java.util.HashMap;
import java.util.UUID;


public class Handler implements RequestHandler<APIGatewayV2HTTPEvent, APIGatewayV2HTTPResponse> {
    private static final String table = System.getenv("TABLE_NAME");

    DynamoDbClient ddb = DynamoDbClient.builder()
            .region(Region.EU_CENTRAL_1)
            .build();
    @Override
    public APIGatewayV2HTTPResponse handleRequest(APIGatewayV2HTTPEvent event, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("EVENT TYPE: "+ event.getClass());
        HelloWorldRequest helloWorldRequest = new Gson().fromJson(event.getBody(), HelloWorldRequest.class);

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        putItemInTable(helloWorldRequest.getRequest(), table);

        APIGatewayV2HTTPResponse build = APIGatewayV2HTTPResponse.builder()
                .withHeaders(headers)
                .withStatusCode(200)
                .withIsBase64Encoded(false)
                .withBody(new Gson().toJson(new HelloWorldResponse(helloWorldRequest.getRequest())))
                .build();

        return build;
    }

    public static void putItemInTable(String name,
                                      String tableName){

        HashMap<String,AttributeValue> itemValues = new HashMap<>();
        itemValues.put("id", AttributeValue.builder().s(UUID.randomUUID().toString()).build());
        itemValues.put("name", AttributeValue.builder().s(name).build());

        DynamoDbClient ddb = DynamoDbClient.builder()
                .region(Region.EU_CENTRAL_1)
                .build();

        PutItemRequest request = PutItemRequest.builder()
                .tableName(tableName)
                .item(itemValues)
                .build();

        try {
            PutItemResponse response = ddb.putItem(request);
            System.out.println(tableName +" was successfully updated. The request id is "+response.responseMetadata().requestId());

        } catch (ResourceNotFoundException e) {
            System.err.format("Error: The Amazon DynamoDB table \"%s\" can't be found.\n", tableName);
            System.err.println("Be sure that it exists and that you've typed its name correctly!");
            System.exit(1);
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }
}
