package org.example;

public class HelloWorldRequest {
    private String request;

    public HelloWorldRequest(String request) {
        this.request = request;
    }

    public HelloWorldRequest() {
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
